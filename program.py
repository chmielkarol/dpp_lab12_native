import ctypes as ct
import os


def main():
    calc = ct.cdll.LoadLibrary('Calc.dll')

    size = int(input("Podaj rozmiar wektorów: "))

    FLOAT = ct.c_float
    PFLOAT = ct.POINTER(FLOAT)
    INT = ct.c_int

    sizeToPass = INT(size)

    flttoarr = FLOAT * size

    calc.add_vectors.argtypes = [flttoarr,
                                  flttoarr,
                                  INT]
    calc.add_vectors.restype = PFLOAT

    calc.subtract_vectors.argtypes = [flttoarr,
                                  flttoarr,
                                  INT]
    calc.subtract_vectors.restype = PFLOAT

    calc.calculate_scalar_product.argtypes = [flttoarr,
                                  flttoarr,
                                  INT]
    calc.calculate_scalar_product.restype = FLOAT

    a = flttoarr()
    b = flttoarr()

    for i in range(size):
        a[i] = int(input("a[" + str(i) + "] = "))

    for i in range(size):
        b[i] = int(input("b[" + str(i) + "] = "))

    while(True):
        print("Wybierz operację")
        print("1. Dodaj wektory")
        print("2. Odejmij wektory")
        print("3. Oblicz iloczyn skalarny")
        option = int(input("Twój wybór: "))

        if option == 1 :
            ret = calc.add_vectors(a, b, sizeToPass)
            print("c = a + b")
            for i in range(size):
                    print("c[i] = ", str(ret[i]))
            break;

        if option == 2 :
            ret = calc.subtract_vectors(a, b, sizeToPass)
            print("c = a - b")
            for i in range(size):
                    print("c[i] = ", str(ret[i]))
            break;

        if option == 3 :
            ret = calc.calculate_scalar_product(a, b, sizeToPass)
            print("a * b = ", ret)
            break;


if __name__ == "__main__":
    main()
